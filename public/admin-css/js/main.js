function ajaxShowOrders(content){
	var id = $(content).data('id');
	var dataHtml = '';
	$('.col-btn-status .col-md-3 button').removeClass('active');
	$(content).addClass('active');
	$.ajax({
		url: '/admin/order/ajax-show-orders',		
		type:'GET',
		data:{id:id},
		success: function( result ) {
			$(".show-ajax-card").html(result);
		}
	});
}