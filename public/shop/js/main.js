function removeItemCart(id,content)
{
	var cartItemRemove = $(content).parents('li.cart_item');
	var form_data = new FormData();
	form_data.append("rowId",id);
	$.ajax({
		url: '/remove-item-cart',
		data: form_data,
		type:'POST',
		contentType: false,
		processData: false,
		success: function( msg ) {
			if(msg[0]['status'] = 'true' ){
				cartItemRemove.remove();
				$('.order_total_amount').html(msg[0]['totalNew'] + ' VNĐ');
			}
		}
	});
}

function ajaxShowCart(content){
	var dataHtml = ''
	$.ajax({
		url: '/ajax-show-cart',		
		type:'GET',
		contentType: false,
		processData: false,
		success: function( result ) {
			console.log(result);
			$(".dataAjaxCart").html(result);
		}
	});
}