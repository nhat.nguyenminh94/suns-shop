<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $price_pd = $this->price;
          $price_pd = (int) str_replace(",",'', $price_pd);
        $this->merge(['price' => $price_pd]);
        return [
            'file_uploads'       => 'required',
            'name'       => 'required|max:255',
            // 'price'     => 'required|numeric',
            'content1'       => 'required',
        ];
    }
    public function messages()
    {
        return [
             'file_uploads.required'       => 'Image không được để trống',
            'name.required'       => 'Name không được để trống',
            // 'price.numeric'       => 'Price phải là số 123',
            'content1.required'       => 'Content không được để trống',
        ];
    }
}
