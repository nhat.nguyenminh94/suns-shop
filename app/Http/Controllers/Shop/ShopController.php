<?php

namespace App\Http\Controllers\Shop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Category;
use Cart;
use App\Models\Order;
use Carbon\Carbon;
use Response;
use View;
class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::select()->where('active', 1)->get();
        return view('shop.index', ['products' => $products]);
    }

    public function allProducts()
    {
        $products = Product::select()->where('active', 1)->paginate(5);
        return view('shop.all_products.all_products', ['products' => $products]);
    }

    /**
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function categoryList($name, $id)
    {
        $categoryDetail = Category::where('id', $id)->get();
        $productList = Product::select()->where('category_id', $categoryDetail[0]['id'])->paginate(5);
        $productList = $productList->total() == 0 ? '400' : $productList;

        return view('shop.category-list.category-list', ['productList' => $productList, 'categoryDetail' => $categoryDetail[0]]);
    }

    public function addProductToCart(Request $request)
    {
        $product = Product::find($request->id);
        if ($product) {
            Cart::add(
                ['id' => $request->id, 
                'name' => $product->name, 
                'qty' => $request->quantity_input, 
                'price' => $product->price, 
                'weight' => 550,
                'options' => ['image' => $product->images->first()->src ?? 'error'
            ]]);
        }
        return redirect()->back();
    }

    public function listCartProduct()
    {
        $products = Cart::content();
        $viewData = [
            'products' => $products
        ];

        return view('shop.cart.show-cart', $viewData);
    }

    public function removeItemCart(Request $request)
    {
        Cart::remove($request->rowId);
        $status = 'true';
        $totalNew = Cart::total();
        $msg[] = ['status' => $status,
            'totalNew' => $totalNew];
        return $msg;
    }

    public function addCart(Request $request)
    {
         $products = Cart::Content();
        if(count($products) == 0){
           return redirect()->back()->with('status', 'Chưa có sản phẩm trong giỏ hàng');
        }
          if($request->use_user == "on"){
            $orderAdd['name'] = auth()->user()->name; 
            $orderAdd['phone'] = auth()->user()->phone; 
            $orderAdd['delivery_address'] = auth()->user()->address; 
        }else{
             $orderAdd = $request->all();
        }
        $orderAdd['checkout_at'] = Carbon::now()->toDateTimeString();
        $orderAdd['user_id'] = auth()->user()->id;
        $total = Cart::total();
        $result = str_replace(",","",$total);
        $orderAdd['total'] = (int)$result;
        $orderAdd['status'] = 1;
        $order = Order::create($orderAdd);
        foreach ($products as $product) {
            $order->orderDetails()->create(['product_id' => $product->id, 'order_id' => $order->id, 'quantity' => $product->qty, 'price' => $product->price]);
        }
        Cart::destroy();
        return redirect()->route('shop.index')->with('status', 'Đơn Hàng của bạn đã được lưu!');
      
    }

     public function ajaxShowCart()
    {
         $products = Cart::Content();
         $total = Cart::total();
           return Response::json(View::make('shop.cart.show-ajax-cart', array('items' => $products, 'total' => $total))->render());
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);
        return view('shop.detail.detail', ['product' => $product]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
