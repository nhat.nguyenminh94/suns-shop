<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use Response;
use View;
use Carbon\Carbon;
class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

         $orders = Order::select()->OrderBy('created_at', 'asc')->paginate(5);
        return view('admin.order.list',
            [
                'orders' => $orders
            ]);
    }

     public function indexStatus($status)
    {
         $orders = Order::select()->where('status',$status)->orderBy('created_at', 'asc')->paginate(5);
        return view('admin.order.list',
            [
                'orders' => $orders , 'statusActive' => (int)$status
            ]);
    }

       public function ajaxShowOrders(Request $request)
    {
        $idOrders =  $request->id;
        if($idOrders != "0"){
          $ordersStatus = Order::select()->where('status',$idOrders)->OrderBy('created_at', 'asc')->paginate(5);
        }else{
            $ordersStatus = Order::select()->OrderBy('created_at', 'asc')->paginate(5);
        }
          return Response::json(View::make('admin.order.show-ajax-order', array('ordersStatus' => $ordersStatus))->render());
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::findOrFail($id);
        return view('admin.order.update',['order'=>$order]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $model = Order::findOrFail($id);
       $dataUpdate = $request->all();
       $dataUpdate['delivery_at'] =  Carbon::parse($request->delivery_at);
       $model->update($dataUpdate);
       return redirect()->route('admin.order.list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
