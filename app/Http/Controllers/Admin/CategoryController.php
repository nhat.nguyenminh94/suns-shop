<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Http\Requests\CategoryRequest;
use App\Models\Product;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $categories = Category::select()->paginate(5);
        return view('admin.category.list',
            [
                'categories' => $categories
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $categories = Category::all();
        return view('admin.category.create',
            [
                'categories' => $categories
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $dataInput = array_merge($request->all(), [
            'parent_id' => $request->parent_id,
        ]);
        Category::create($dataInput);
        return redirect()->route('admin.category.list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
        public function edit($id)
    {

        $categories = Category::select()->where('id', '!=', $id)->get();
        $category = Category::findOrFail($id);
        // dd($categories);
        return view('admin.category.update',
            [
                'categories' => $categories,
                'category' => $category,
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {
        $category = Category::findOrFail($id);
        $dataInput = $request->all();
        $category->update($dataInput);
        return redirect()->route('admin.category.list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(CategoryRequest $request)
    {
        // $request->all();
        // dd(  $request->all());
        $category = Category::findOrFail($request->idDelete);
        if($category->parent_id != 0 ){
            $category->delete(); 
        }
        return redirect()->route('admin.category.list');
    }
}
