<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Validator;
use App\Models\Image;
use File;
use App\Http\Requests\ProductRequest;
class ProductController extends Controller
{
    protected $categories;

    public function __construct()
    {
        $this->categories = Category::select()->get();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $products = Product::select()->paginate(5);
        return view('admin.product.list',
            [
                'products' => $products
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $categories = Category::where('parent_id', '!=' , 0)->get();
        return view('admin.product.create',
            [
                'categories' => $categories
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(ProductRequest $request)
    {
        $this->handleStore($request);
        return redirect()->route('admin.product.list');
    }

    public function handleStore($request)
    {

        $dataInput = array_merge($request->all(), [
            'content' => $request->content1,
        ]);
        $product = Product::create($dataInput);
        foreach ($request->file_uploads as $key => $val) {
            $dataImage['src'] = $val['src'];
            $dataImage['alt'] = str_slug($request->title)."-hinh-".$key;
            $dataImage['description'] = str_slug($request->title)."-hinh-".$key;
            $product->images()->create($dataImage);
        }
        return $product;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          $categories = $this->categories;
        $product = Product::findOrFail($id);
        return view('admin.product.update', ['product' => $product,
            'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(ProductRequest $request, $id)
    {
        $model = Product::findOrFail($id);
        $this->handleUpdate($request, $model);
        return redirect()->route('admin.product.list');
    }

    public function handleUpdate($request, $model)
    {
        // dd($request->all());
        $dataInput = array_merge($request->all(), [
            'content' => $request->content1,
        ]);
        $product = $model->update($dataInput);
        $imagesModel = $model->images()->pluck('id')->toArray();
        $dataRequest = $request->file_uploads;
        foreach ($dataRequest as $key => $value) {
            if (isset($value['id'])) {
                $idProduct = $value['id'];
                $imageUpdate = $model->images()->find($idProduct);
                $oldImageSrc = $imageUpdate->src;
                $pos = array_search($value['id'], $imagesModel);
                unset($value['id']);
                if ($imageUpdate->update(['src' => $value['src']])) {
                    if ($oldImageSrc != $value['src']) {
                        File::delete($oldImageSrc);
                    }
                }
                unset($dataRequest[$key]);
                unset($imagesModel[$pos]);
            } else {
                $model->images()->create(['src' => $value['src']]);
            }
        }
        if (sizeof($imagesModel) > 0) {
            foreach ($imagesModel as $itemDelete) {
                $imageDelete = $model->images()->find($itemDelete);
                if ($imageDelete->delete()) {
                    $oldImageSrc = $imageDelete->src;
                    File::delete($imageDelete);
                }
            }
        }
        return $product;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

        public function UploadMultiple(Request $request, $type = null)
    {
        $success = [];
        $errors = [];
        foreach ($request->file('files') as $key => $file) {
            $handle = $this->handleUploadMultiple($request, $key, $type);
            if ($handle['status'] == false) {
                $errors[] = $handle;
            }
            if ($handle['status'] == true) {
                $success[] = $handle;
            }
        }
        return [
            'success' => $success,
            'errors' => $errors,
        ];
    }

    public function handleUploadMultiple($request, $key, $type = null)
    {
        $validator = Validator::make($request->all(),
            [
                'files.' . $key => 'image',
            ],
            [
                'files.' . $key . '.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)',
            ]);
        if ($validator->fails()) {
            return [
                'status' => false,
                'file' => $request->file('files')[$key]->getClientOriginalName() ?? null,
                'errors' => $validator->errors()->getMessages()['files.' . $key],
            ];
        }
        $file = $request->file('files')[$key];
        if ($type) {
            $dir = 'uploads/' . $type . '/';
        } else {
            $dir = 'uploads/';
        }
        $date = date("dmY");
        $originName = $file->getClientOriginalName();
        $file_name = rand() . $date . '.' . $file->getClientOriginalExtension();
//        $file = request()->file('file');
//        $name = time() . $file->getClientOriginalName();
//        $filePath = 'products/' . $name;
//        Storage::disk('s3')->put($filePath, file_get_contents($file), 'public');
        if ($file->move($dir, $file_name)) {
            return ['status' => true, 'filename' => $dir . $file_name, 'filenameorigin' => $originName];
        }
        return ['status' => false, 'errors' => $originName];
    }
}
