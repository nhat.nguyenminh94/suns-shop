<?php
namespace App\Helpers;
use App\Models\Category;
use App\Models\Product;

class Helper
{
	public static function showCategoryMenu(){
		$categories = Category::select()->where('parent_id',0)->get();
		return $categories;
	}
	public static function showProductsSliders(){
		$products = Product::select()->where('status',1)->get();
		return $products;
	}

	public static function textCut($str, $length)
    {
        $str = strip_tags($str);
        $str = html_entity_decode($str, ENT_QUOTES, "utf-8");
        $str = str_replace(["\n","\r"],"",$str);

        if (strlen($str) > $length) {
            $str = mb_substr($str, 0, $length + 1, 'UTF-8');
            $pos = strrpos($str, ' ');
            $str = mb_substr($str, 0, ($pos > 0) ? $pos : $length, 'UTF-8');
            $str = $str . "...";
        }
        return $str;
    }
}
