<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable=['name','price','content','active','category_id'];
  	public function category()
   {
   		return $this->belongsTo('App\Models\Category','category_id');
   }
     public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class,'product_id');
    }
}
