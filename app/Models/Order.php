<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $fillable=['user_id','checkout_at','delivery_address','delivery_at','name','phone','total','status'];
   public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class,'order_id');
    }
      public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
