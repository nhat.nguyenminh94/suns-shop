<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('shop.layouts.master');
// });
Route::group(['prefix'=> '', 'namespace' => 'Shop' , 'as' => 'shop.'], function(){
     Route::get('/', 'ShopController@index')->name('index');
     Route::get('/the-loai/{slugParent}/{slug}', 'ShopController@categoryList')->name('categorylist');
     Route::get('/bai-viet/chi-tiet/{id}', 'ShopController@show')->name('detail');
     Route::get('/all-products', 'ShopController@allProducts')->name('allproducts');
     Route::get('/category-list/{name}_{id}', 'ShopController@categoryList')->name('categorylist');
     Route::post('/add-product-cart', 'ShopController@addProductToCart')->name('addproducttocart');
     Route::get('/gio-hang-cua-ban','ShopController@listCartProduct')->name('listcart');
     Route::post('/remove-item-cart','ShopController@removeItemCart')->name('removeitemcart');
     Route::post('/add-cart','ShopController@addCart')->name('addcart');
     Route::get('/ajax-show-cart','ShopController@ajaxShowCart')->name('ajaxshowcart');
     Route::post('/comment/add/{slug}_{id}', 'ShopController@addComment')->name('addcomment');
     Route::get('/about-us', 'ShopController@aboutUs')->name('about');
     Route::get('/contact-us', 'ShopController@contactUs')->name('contact');
     Route::post('/contact-us/add', 'ShopController@addContact')->name('addcontact');
});
Route::group(['prefix' => 'admin', 'namespace' => 'Admin','as'=>'admin.','middleware'=>['auth','checktype']], function () {
    Route::get('/', 'AdminController@index')->name('index');
     Route::group(['prefix' => 'category','as'=>'category.'], function () {
        Route::get('/', 'CategoryController@index')->name('list');
        Route::get('/add', 'CategoryController@create')->name('add');
        Route::post('/created', 'CategoryController@store')->name('created');
        Route::get('edit/{id}', 'CategoryController@edit')->name('edit');
        Route::post('/{id}', 'CategoryController@update')->name('update');
        Route::delete('/deletecate', 'CategoryController@destroy')->name('destroy');
    });
    Route::group(['prefix' => 'product','as'=>'product.'], function () {
        Route::get('/', 'ProductController@index')->name('list');
        Route::get('/add', 'ProductController@create')->name('add');
        Route::post('/created', 'ProductController@store')->name('created');
        Route::get('edit/{id}', 'ProductController@edit')->name('edit');
        Route::post('/{id}', 'ProductController@update')->name('update');
        Route::delete('/product/{id}', 'ProductController@destroy')->name('destroy');
        Route::post('/deleteimage', 'ProductController@deleteImage')->name('deleteimage');
        Route::post('/uploadimage/{type?}', 'ProductController@UploadMultiple')->name('uploadimage');
    });
       Route::group(['prefix' => 'order','as'=>'order.'], function () {
        Route::get('/', 'OrderController@index')->name('list');
        Route::get('/{status}', 'OrderController@indexStatus')->name('liststatus');
        Route::get('/ajax-show-orders','OrderController@ajaxShowOrders')->name('ajaxshoworders');
        Route::get('edit/{id}', 'OrderController@edit')->name('edit');
        Route::post('/{id}', 'OrderController@update')->name('update');
    });
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
