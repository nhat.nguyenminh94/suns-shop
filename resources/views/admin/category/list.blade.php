@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Categories</h1>
    <p class="mb-4"></p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Categories Table</h6>
        </div>
        <div class="card-header py-3">
            <div class="col-md-2">
                <a class="btn btn-success text-center" href="{{route('admin.category.add')}}"
                   style="width: 100%;margin-bottom: 10px;">
                    Add </a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Active</th>
                        <th>Parent ID</th>
                        <th colspan="2">Control</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Active</th>
                        <th>Parent ID</th>
                        <th colspan="2">Control</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($categories as $category)
                        <tr>
                            <td>{{$category->id}}</td>
                            <td>{{$category->name}}</td>
                            <td>@if($category->active == 1 )
                                    Show
                                @else
                                    Hidden
                                @endif</td>
                            <td>
                                {{$category->parent->name ?? "Error Or No Parent"}}
                            </td>
                            <td>
                                <a class="btn btn-primary"
                                   href="{{route('admin.category.edit',['id'=>$category->id])}}">Update</a>
                            </td>
                        @if(isset($category->children[0]->name) || $category->product()->count() > 0)
                                <td></td>
                            @else
                                <td>
                                    <button type="button" class="btn btn-danger openModalDelete" data-toggle="modal"
                                            data-target="#exampleModalCenter" data-id="{{$category->id}}">
                                        Delete
                                    </button>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="row col-md-12 justify-content-center">
                {{ $categories->links() }}
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Nếu chắc chắn xóa vui lòng nhấn nút Delete <br>
                    <b style="color:#E02D1B;">Lưu ý khi xóa thể loại sẽ không thể phục hồi</b>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <form action="{{route('admin.category.destroy')}}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button class="btn btn-danger" id="idDelete" name="idDelete" value="">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript" charset="utf-8" async defer>
        $(document).on("click", ".openModalDelete", function () {
            var myBookId = $(this).data('id');
            $(".modal-content #idDelete").val(myBookId);
            $(".modal-content #exampleModalLongTitle").html('Bạn Chắc Chắn muốn xóa thể loại số ' + myBookId + ' chứ ?');
            // As pointed out in comments,
            // it is unnecessary to have to manually call the modal.
        });
    </script>

@endsection