@extends('admin.layouts.master')
@section('styles')
    <style type="text/css" media="screen">
        select option {
            color: #005f89;
        }

        label {
            text-transform: capitalize;
        }

        .swal-text {
            margin-bottom: 20px;
        }

        .swal-footer {
            background-color: rgb(245, 248, 250);
            text-align: center;
            border-top: 1px solid #E9EEF1;
            overflow: hidden;
        }

        @media screen and (max-width: 768px) {
            .product_image {
                text-align: -webkit-center;
            }

            .box_img {
                margin: 20px;
                text-align: -webkit-center;
            }

            .box_href {
                margin-bottom: 20px;
            }
        }
    </style>
    <link rel='stylesheet' href='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css'>
    <script src="/ckeditor/ckeditor.js"></script>
@endsection
@section('content')
    <div class="container">
        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-lg-12">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Create Product!</h1>
                            </div>
                            <form class="user" method="post" action="{{route('admin.product.created')}}"
                                  enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="Categories">Categories</label>
                                    <select class="form-control" id="" name="category_id">
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="Name">Name</label>
                                    <input type="text" class="form-control form-control-user" id="" name="name"
                                          value="{{old('name')}}" placeholder="name">
                                </div>
                                <div class="form-group">
                                    <label for="Price">Price</label>
                                    <input type="text" class="form-control form-control-user price_product" id=""
                                           name="price" value="{{old('price')}}"
                                           placeholder="Price">
                                </div>
                                <div class="form-group">
                                    <label for="content">content</label>
                                    <textarea name="content1" id="content1" >{{old('content1')}}</textarea>
                                    <script> CKEDITOR.replace('content1');</script>
                                </div>
                                <div class="form-group row">
                                    <div class="grid col-lg-4">
                                        <p class="grid-header">
                                            Upload Image
                                            <span class="text-danger">*</span>
                                        </p>
                                        <div class="grid-body">
                                            <div class="row showcase_row_area">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input"
                                                           onchange="UploadImageMultiple(this)" multiple
                                                           id="customFile">
                                                    <label class="custom-file-label" for="customFile">Choose
                                                        file</label>
                                                </div>
                                            </div>
                                            <div class="row showcase_row_area">
                                                <div class="custom-file">
                                                    <div class="progress">
                                                        <div class="progress-bar bg-success progressBarUploadImage"
                                                             role="progressbar" style="width: 0%" aria-valuenow="0"
                                                             aria-valuemin="0" aria-valuemax="100"><span
                                                                    class="progressBarUploadImageShow">0%</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row showcase_row_area errorUploadImageResult">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="grid col-lg-8">
                                        <p class="grid-header">
                                            List Image
                                        </p>
                                        <div class="grid-body ">
                                            <div class="list-unstyled listUploadImageResult">
                                                @php
                                                    $keyImage = 1;
                                                    $dataImage = old('file_uploads',isset($product->images) ? $product->images()->select(['src','status','id'])->get()->toArray() : [] );
                                                @endphp
                                                @foreach($dataImage as $valueImage)
                                                    <div class="media imageResultShow row">
                                                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 product_image">
                                                            <img class="mr-3" src="{{url($valueImage['src'])}}"
                                                                 onclick="showOneImage(this)"
                                                                 data-src="{{url($valueImage['src'])}}" data-atl=""
                                                                 data-height="100%" data-width="100%"
                                                                 style="width:100%;max-width: 200px;height: auto;">
                                                        </div>
                                                        <div class="media-body col-xl-10 col-lg-10 col-md-10 col-sm-12 box_img">
                                                            <div class="form-group">
                                                                <div class="row col-md-12">
                                                                    <div class="col-md-9 box_href">
                                                                        <input type="text"
                                                                               class="form-control enable-mask inputSrc"
                                                                               value="{{url($valueImage['src'])}}"
                                                                               disabled>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <button type="button"
                                                                                class="btn btn-outline-danger"
                                                                                onclick="removePedding(this)">Remove
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @isset($valueImage['id'])
                                                                <input name="file_uploads[{{$keyImage}}][id]"
                                                                       type="hidden" value="{{$valueImage['id']}}"/>
                                                            @endisset
                                                            <input name="file_uploads[{{$keyImage}}][src]"
                                                                   type="hidden" value="{{$valueImage['src']}}"/>
                                                        </div>
                                                    </div>
                                                    <div style="font-size: 0;line-height: 0;margin: 10px 0;">
                                                        &nbsp;
                                                    </div>
                                                    @php
                                                        $keyImage++;
                                                    @endphp
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <legend class="col-form-label col-sm-2 pt-0">Active</legend>
                                    <div class="col-sm-10">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="active" id="gridRadios1"
                                                   value="1" checked>
                                            <label class="form-check-label" for="gridRadios1">
                                                Show
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="active" id="gridRadios2"
                                                   value="2">
                                            <label class="form-check-label" for="gridRadios2">
                                                Hidden
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
 <!--    <script src="/mask-js/jquery.mask.js"></script> -->
 <script src="/js/fm_money/simple.money.format.js"></script>

    <script type="text/javascript">
        // $('.price_product').simpleMoneyFormat();
        
        (function ($, window, undefined) {
            //is onprogress supported by browser?
            var hasOnProgress = ("onprogress" in $.ajaxSettings.xhr());

            //If not supported, do nothing
            if (!hasOnProgress) {
                return;
            }

            //patch ajax settings to call a progress callback
            var oldXHR = $.ajaxSettings.xhr;
            $.ajaxSettings.xhr = function () {
                var xhr = oldXHR.apply(this, arguments);
                if (xhr instanceof window.XMLHttpRequest) {
                    xhr.addEventListener('progress', this.progress, false);
                }

                if (xhr.upload) {
                    xhr.upload.addEventListener('progress', this.progress, false);
                }

                return xhr;
            };
        })(jQuery, window);

        function UploadImageMultiple(e) {
            console.log(e);
            var form_data = new FormData();
            var ins = e.files.length;
            for (var x = 0; x < ins; x++) {
                form_data.append("files[]", e.files[x]);
            }
            $('.progressBarUploadImage').attr('aria-valuenow', 0).css('width', 0 + '%');
            $('.progressBarUploadImageShow').text(0 + '%');
            form_data.append('_token', "{{ csrf_token() }}");
            $.ajax({
                url: "{{route('admin.product.uploadimage',['type'=>'products'])}}",
                data: form_data,
                type: 'POST',
                contentType: false,
                processData: false,
                progress: function (e) {
                    $('.progressBarUploadImageShow').parent().removeClass("bg-danger")
                    $('.progressBarUploadImageShow').parent().addClass("bg-success")
                    if (e.lengthComputable) {
                        var pct = (e.loaded / e.total) * 100;
                        $('.progressBarUploadImage').attr('aria-valuenow', parseInt(pct)).css('width', parseInt(pct) + '%');
                        $('.progressBarUploadImageShow').text(parseInt(pct) + '%');
                        if (parseInt(pct) == 100) {
                            $('.progressBarUploadImageShow').text('complete');
                        }
                    }
                },
                success: function (res) {
                    console.log(res);
                    if (res.errors.length != 0) {
                        $('.errorUploadImageResult').html(printErrorsMultiple(res.errors))
                    }
                    if (res.success.length != 0) {
                        $('.listUploadImageResult').append(printSuccessMultiple(res.success))
                    }
                    $(e).val('')
                }, error: function (error) {
                    $('.progressBarUploadImageShow').parent().removeClass("bg-success")
                    $('.progressBarUploadImageShow').parent().addClass("bg-danger")
                    $('.progressBarUploadImageShow').text('error');
                }
            });
        }

        function printErrorsMultiple(param) {
            var result = '<div class="alert alert-danger dismissible-alert" role="alert">'
                + ' <b>Errors:</b></i>'
                + ' <br>'
                + ' <ul class="list-unstyled">';
            param.forEach(function (element) {
                result = result + '<li>' + element.file + '</li>';
            });
            result = result + '</ul></div>';
            return result;
        }

        var countImageBlog = $('.listUploadImageResult').find('.media').length + 1;

        function printSuccessMultiple(param) {
            var result = '';
            param.forEach(function (element) {
                result = result + '<div class="media imageResultShow row">'
                    + '<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 product_image">'
                    + ' <img src="/' + element.filename + '" onclick="showOneImage(this)" data-src="/' + element.filename + '" data-atl="" data-height="100%" data-width="100%" class="mr-3" style="width:100%;max-width: 200px;">'
                    + '</div>'
                    + ' <div class="media-body col-xl-10 col-lg-10 col-md-10 col-sm-12 box_img">'
                    + ' <div class="form-group">'
                    + ' <div class="row col-md-12"> <div class="col-md-9 box_href">'
                    + ' <input type="text" class="form-control enable-mask inputSrc" value="' + location.protocol + "//" + window.location.host + '/' + element.filename + '" disabled>'
                    + ' </div><div class="col-md-3"><button type="button" class="btn btn-outline-danger" onclick="removePedding(this)" data-src="' + element.filename + '">Remove</button></div> '
                    + ' </div>'
                    + ' </div>'
                    + ' <input type="hidden" name="file_uploads[' + countImageBlog + '][src]" value="' + element.filename + '"/>'
                    + ' </div>'
                    + ' </div>'
                    + '<div style="font-size: 0;line-height: 0;margin: 10px 0;">&nbsp;</div>';
                countImageBlog = countImageBlog + 1;
            });
            return result;
        }


        function removePedding(e) {
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                buttons: {
                    cancel: true,
                    confirm: {
                        text: "Delete",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: true
                    }
                },
            }).then((result) => {
                if (result == true) {
                    swal(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                    $(e).parents('.imageResultShow').remove();
                }
            });
        }

    </script>
@endsection