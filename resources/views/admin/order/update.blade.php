@extends('admin.layouts.master')
@section('content')
 <div class="container">
    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Update Order!</h1>
              </div>
              <form class="user" method="post" action="{{route('admin.order.update',['id'=>$order->id])}}">
              	@csrf
                <div class="form-group">
               	  <label for="name">Tên Người Nhận</label>
                  <input type="text" class="form-control form-control-user" id="" name="name" placeholder="Name" value="{{isset($order) ? $order->name : '' }}">
                </div>
                <div class="form-group">
                  <label for="name">User</label>
                  <input type="text" class="form-control form-control-user" id="" name="user_id" placeholder="User" value="{{isset($order->user) ? $order->user->name : '' }}" disabled>
                </div>
                 <div class="form-group">
                  <label for="name">Địa chỉ giao hàng</label>
                  <input type="text" class="form-control form-control-user" id="" name="delivery_address" placeholder="Delivery" value="{{isset($order->delivery_address) ? $order->delivery_address : '' }}">
                </div>
                <div class="form-group">
                  <label for="name">Số điện thoại liên  lạc</label>
                  <input type="text" class="form-control form-control-user" id="" name="phone" placeholder="User" value="{{isset($order->phone) ? $order->phone : '' }}">
                </div>
                 <div class="form-group">
                  <label for="name">Tổng tiền đơn hàng</label>
                  <input type="text" class="form-control form-control-user" id="" name="total" placeholder="User" value="{{isset($order->total) ? number_format($order->total,0,',','.') : '' }}" disabled>
                </div>
                 <div class="form-group">
                  <label for="name">Ngày giao hàng</label>
                 
                  <input type="date" class="form-control form-control-user" id="" name="delivery_at" placeholder="Date" value="{{date('Y-m-d',strtotime($order->delivery_at))}}" >
                </div>
                <div class="form-group row">
                    <legend class="col-form-label col-sm-2 pt-0">Status</legend>
                     <div class="col-sm-10">
                        <div class="form-check">
                              <input class="form-check-input" type="radio" name="status" id="gridRadios1"
                                     value="1" @if( isset($order) && $order->status == 1 )
                                     checked
                                     @elseif(!isset($order))
                                     checked
                                      @endif>
                              <label class="form-check-label" for="gridRadios1">
                                  Pendding
                              </label>
                          </div>
                          <div class="form-check">
                              <input class="form-check-input" type="radio" name="status" id="gridRadios2"
                                     value="2" @if(isset($order) && $order->status == 2 )
                                     checked
                                      @endif>
                              <label class="form-check-label" for="gridRadios2">
                                 Success
                              </label>
                          </div>
                           <div class="form-check">
                              <input class="form-check-input" type="radio" name="status" id="gridRadios2"
                                     value="3" @if(isset($order) && $order->status == 3 )
                                     checked
                                      @endif>
                              <label class="form-check-label" for="gridRadios2">
                                 Cancel
                              </label>
                          </div>
                      </div>
                  </div>
                <div class="form-group">
                 <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
              <hr>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
@endsection