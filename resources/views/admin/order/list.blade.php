@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Orders</h1>
    <p class="mb-4"></p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Orders Table</h6>
        </div>
        <div class="card-header py-3">
        	<div class="row col-btn-status">
            <div class="col-md-3">
                <a href="{{route('admin.order.list')}}" class="btn btn-outline-primary {{!isset($statusActive) ? 'active' : ''}}"
                   style="width: 100%;margin-bottom: 10px;" data-id="0" >
                    All Orders </a>
            </div>
             <div class="col-md-3">
                <a href="{{route('admin.order.liststatus',['status'=>1])}}" class="btn btn-outline-info {{isset($statusActive) && $statusActive === 1 ? 'active' : ''}}"
                   style="width: 100%;margin-bottom: 10px;" data-id="1" >
                    Pending Orders </a>
            </div>
             <div class="col-md-3">
                <a href="{{route('admin.order.liststatus',['status'=>2])}}" class="btn btn-outline-success {{isset($statusActive) && $statusActive === 2 ? 'active' : ''}}"
                   style="width: 100%;margin-bottom: 10px;" data-id="2">
                    Success Orders </a>
            </div>
             <div class="col-md-3">
                <a href="{{route('admin.order.liststatus',['status'=>3])}}" class="btn btn-outline-danger {{isset($statusActive) && $statusActive === 3 ? 'active' : ''}}"
                   style="width: 100%;margin-bottom: 10px;" data-id="3">
                    Cancel Orders </a>
            </div>
        	</div>
        </div>
        <div class="card-body show-ajax-card">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Address</th>
                        <th>Check out</th>
                        <th>Status</th>
                        <th colspan="2">Control</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Address</th>
                        <th>Check out</th>
                        <th>Status</th>
                        <th colspan="2">Control</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($orders as $order)
                        <tr>
                            <td>{{$order->id}}</td>
                            <td>{{$order->name}}</td>
                            <td>{{$order->phone}}</td>
                            <td>{{$order->delivery_address}}</td>
                            <td>{{date('d/m/Y H:i', strtotime($order->checkout_at))}}</td>
                            <?php $status =  $order->status ?>
                            @if($status == 1 )
                            <td>
                            	<button type="button" class="btn btn-info">Pending</button>
                            </td>
                            @elseif($status == 2)
                              <td>
                            	<button type="button" class="btn btn-success">Success</button>
                            </td>
                            @else
                             <td>
                            	<button type="button" class="btn btn-danger">Cancel</button>
                            </td>
                            @endif
                            <td>
                                <a class="btn btn-primary"
                                   href="{{route('admin.order.edit',['id'=>$order->id])}}">Update</a>
                            </td>
              
                               <!--  <td>
                                    <button type="button" class="btn btn-danger openModalDelete" data-toggle="modal"
                                            data-target="#exampleModalCenter" data-id="#">
                                        Delete
                                    </button>
                                </td> -->
     
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="row col-md-12 justify-content-center">
                {{ $orders->links() }}
            </div>
        </div>
    </div>

@endsection