@if($ordersStatus[0] === null)
 <div class="table-responsive">
    <div class="col-md-12">
        <button type="button" class="btn btn-danger">Status này hiện chưa có đơn hàng</button>
    </div>
 </div>
 @else
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Address</th>
                        <th>Check out</th>
                        <th>Status</th>
                        <th colspan="2">Control</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Address</th>
                        <th>Check out</th>
                        <th>Status</th>
                        <th colspan="2">Control</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($ordersStatus as $order)
                        <tr>
                            <td>{{$order->id}}</td>
                            <td>{{$order->name}}</td>
                            <td>{{$order->phone}}</td>
                            <td>{{$order->delivery_address}}</td>
                            <td>{{date('d/m/Y H:i', strtotime($order->checkout_at))}}</td>
                            <?php $status =  $order->status ?>
                            @if($status == 1 )
                            <td>
                                <button type="button" class="btn btn-info">Pending</button>
                            </td>
                            @elseif($status == 2)
                              <td>
                                <button type="button" class="btn btn-success">Success</button>
                            </td>
                            @else
                             <td>
                                <button type="button" class="btn btn-danger">Cancel</button>
                            </td>
                            @endif
                            <td>
                                <a class="btn btn-primary"
                                   href="{{route('admin.order.edit',['id'=>$order->id])}}">Update</a>
                            </td>
              
                                <!-- <td>
                                    <button type="button" class="btn btn-danger openModalDelete" data-toggle="modal"
                                            data-target="#exampleModalCenter" data-id="#">
                                        Delete
                                    </button>
                                </td> -->
     
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="row col-md-12 justify-content-center">
                {{ $ordersStatus->links() }}
            </div>
        
@endif