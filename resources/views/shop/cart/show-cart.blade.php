@extends('shop.layouts.master',[ 'single_page' =>'true' ])
@section('styles')
    <link rel="stylesheet" type="text/css" href="/shop/styles/cart_styles.css">
    <link rel="stylesheet" type="text/css" href="/shop/styles/cart_responsive.css">
    <link rel="stylesheet" type="text/css" href="/shop/styles/contact_styles.css">
    <link rel="stylesheet" type="text/css" href="/shop/styles/contact_responsive.css">
    <style type="text/css">
        .product_image .product_image_child {
            max-width: 115px !important;
            max-height: 115px !important;
        }

        .viewed, .adverts {
            display: none;
        }

        .brands .brands_slider_container {
            display: none;
        }

        .contact_form {
            padding-top: 0px;
        }
    </style>
@endsection
@section('content')
    <div class="cart_section">
        <div class="container">
                @if (session('status'))
    <div class="alert alert-danger" role="alert">
       <strong> {{ session('status') }} </strong>
    </div>
@endif
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <div class="cart_container">
                        <div class="cart_title">Shopping Cart</div>
                        <div class="cart_items">
                            <ul class="cart_list">
                                @foreach($products as $product)
                                    <li class="cart_item clearfix">
                                        <div class="cart_item_image"><img
                                                    src="{{$product->images[0]->src ??  '/shop/images/shopping_cart.jpg'}}"
                                                    alt=""></div>
                                        <div class="cart_item_info d-flex flex-md-row flex-column justify-content-between">
                                            <div class="cart_item_name cart_info_col">
                                                <div class="cart_item_title">Name</div>
                                                <div class="cart_item_text">{{$product->name}}</div>
                                            </div>
                                            <div class="cart_item_color cart_info_col">
                                                <div class="cart_item_title">Color</div>
                                                <div class="cart_item_text"><span
                                                            style="background-color:#999999;"></span>Silver
                                                </div>
                                            </div>
                                            <div class="cart_item_quantity cart_info_col">
                                                <div class="cart_item_title">Quantity</div>
                                                <div class="cart_item_text">{{ $product->qty }}</div>
                                            </div>
                                            <div class="cart_item_price cart_info_col">
                                                <div class="cart_item_title">Price</div>
                                                <div class="cart_item_text">{{ number_format($product->price,0,',','.') }}</div>
                                            </div>
                                            <div class="cart_item_total cart_info_col">
                                                <div class="cart_item_title">Total</div>
                                                <div class="cart_item_text">{{ number_format($product->total,0,',','.') }}</div>
                                            </div>
                                            <div class="cart_item_total cart_info_col">
                                                <div class="cart_item_title">Xóa khỏi giỏ hàng</div>
                                                <div class="cart_item_text">
                                                    <button type="button" class="btn btn-danger btnRemoveItem"
                                                            onclick="removeItemCart('{{$product->rowId}}',this)"
                                                            data-id="{{$product->rowId}}">
                                                        Xóa
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                        <!-- Order Total -->
                        <div class="order_total">
                            <div class="order_total_content text-md-right">
                                <div class="order_total_title">Order Total:</div>
                                <div class="order_total_amount">{{Cart::total()}} VNĐ</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="contact_form">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <div class="contact_form_container">
                        <div class="contact_form_title">Đơn Hàng</div>
                      
                        <form action="{{route('shop.addcart')}}" method="post" id="contact_form">
                            @csrf
                            <div class="form-check col-md-12 ">
                                <input type="checkbox" class="form-check-input use-detail-user" name="use_user" id="use_user" onchange="checkedUserBox(this)">
                                <label class="form-check-label" for="exampleCheck1">Sử dụng thông tin tài khoản để giao hàng</label>
                            </div>
                            <div class="contact_form_inputs d-flex flex-md-row flex-column justify-content-between align-items-between">
                                <input type="text" id="delivery_address" name="delivery_address"
                                class="contact_form_email input_field" placeholder="delivery address"
                                required="required" data-error="delivery_address is required.">
                                <input type="text" id="name" name="name"
                                class="contact_form_email input_field" placeholder="name"
                                required="required" data-error="name is required."> 
                                <input type="text" id="phone" name="phone"
                                class="contact_form_email input_field" placeholder="phone"
                                required="required" data-error="phone is required.">                            
                            </div>


                            <div class="contact_form_button">
                                <button type="submit" class="button contact_submit_button">Thanh Toán</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <div class="panel"></div>
    </div>
@endsection
@section('scripts')
    <script src="/shop/js/cart_custom.js"></script>
    <script src="/shop/js/contact_custom.js"></script>
    <script src="/shop/js/main.js"></script>
    <script src="/js/fm_money/simple.money.format.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.product_price').simpleMoneyFormat();
             if($('#use_user').is(':checked') == true){
                 $('.contact_form_inputs input').prop('disabled', true);
             }
        });


        function checkedUserBox(content){
            // console.log($('#use_user').is(':checked'));
            if(content.checked || $('#use_user').is(':checked') == true){
                $('.contact_form_inputs input').prop('disabled', true);
            }else{
                 $('.contact_form_inputs input').prop('disabled', false);
            }
        }

    </script>
@endsection