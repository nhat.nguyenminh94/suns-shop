
  @foreach($items as $item)
  <div class="row"> 
    <div class="col-lg-3">
      <img src="{{$item->options->image ?? 'error'}} "  class="img-thumbnail">
  </div>
  <div class="col-lg-3">
      <p class="text-center"> Tên SP </p>
      <p class="text-center"> {{$item->name}} </p>
  </div>
  <div class="col-lg-3">
      <p class="text-center"> Số Lượng </p>
      <p class="text-center"> {{$item->qty}} </p>
  </div>
  <div class="col-lg-3">
      <p class="text-center"> Tổng tiền </p>
      <p class="text-center"> {{number_format($item->price * $item->qty,0,',','.')}} </p>
  </div>
</div>
@endforeach
<div class="row">     
   <div class="col-lg-12">     
       <p class="text-right"> Tổng tiền giỏ hàng </p>
       <p class="text-right">  {{$total}}  </p>
   </div>
</div>