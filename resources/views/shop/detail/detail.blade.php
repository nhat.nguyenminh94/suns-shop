@extends('shop.layouts.master',[ 'single_page' =>'true' ])
@section('styles')
<link rel="stylesheet" type="text/css" href="/shop/styles/product_styles.css">
<link rel="stylesheet" type="text/css" href="/shop/styles/product_responsive.css">
@endsection
@section('content')
<div class="single_product">
		<div class="container">
			<div class="row">

				<!-- Images -->
				<div class="col-lg-2 order-lg-1 order-2">
					<ul class="image_list">
						@foreach($product->images as $image)
						<li data-image="{{url($image->src)}}"><img src="{{url($image->src)}}" alt=""></li>
						@endforeach
						<!-- <li data-image="images/single_4.jpg"><img src="images/single_4.jpg" alt=""></li>
						<li data-image="images/single_2.jpg"><img src="images/single_2.jpg" alt=""></li>
						<li data-image="images/single_3.jpg"><img src="images/single_3.jpg" alt=""></li> -->
					</ul>
				</div>

				<!-- Selected Image -->
				<div class="col-lg-5 order-lg-2 order-1">
					<div class="image_selected"><img src="{{url($product->images[0]->src)}}" alt=""></div>
				</div>

				<!-- Description -->
				<div class="col-lg-5 order-3">
					<div class="product_description">
						<div class="product_category">{{$product->category->name}}</div>
						<div class="product_name">{{$product->name}}</div>
						<div class="product_text"><p>{!! $product->content !!}</p></div>
						<div class="order_info d-flex flex-row">
							@if(Auth::check())
							<form  method="post" action="{{route('shop.addproducttocart')}}">
							@else
							<form  method="get" action="/login">
							@endif
								@csrf
								<div class="clearfix" style="z-index: 1000;">

									<!-- Product Quantity -->
									<div class="product_quantity clearfix">
										<span>Quantity: </span>
										<input id="quantity_input" type="text" pattern="[0-9]*" value="1" name="quantity_input">
										<div class="quantity_buttons">
											<div id="quantity_inc_button" class="quantity_inc quantity_control"><i class="fas fa-chevron-up"></i></div>
											<div id="quantity_dec_button" class="quantity_dec quantity_control"><i class="fas fa-chevron-down"></i></div>
										</div>
									</div>

									<!-- Product Color -->
									<ul class="product_color">
										<li>
											<span>Color: </span>
											<div class="color_mark_container"><div id="selected_color" class="color_mark"></div></div>
											<div class="color_dropdown_button"><i class="fas fa-chevron-down"></i></div>

											<ul class="color_list">
												<li><div class="color_mark" style="background: #999999;"></div></li>
												<li><div class="color_mark" style="background: #b19c83;"></div></li>
												<li><div class="color_mark" style="background: #000000;"></div></li>
											</ul>
										</li>
									</ul>

								</div>
								<input type="hidden" name="id" value="{{$product->id}}">
								<div class="product_price">{{$product->price}}</div> VNĐ
								<div class="button_container">
										
										<button type="submit" class="button cart_button">Add to Cart</button>
									<div class="product_fav"><i class="fas fa-heart"></i></div>
								</div>
								
							</form>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
@endsection
@section('scripts')
<script src="/shop/js/product_custom.js"></script>
 <script src="/js/fm_money/simple.money.format.js"></script>
 <script type="text/javascript">
 	 $( document ).ready(function() {
        $('.product_price').simpleMoneyFormat();
    });
 </script>
@endsection