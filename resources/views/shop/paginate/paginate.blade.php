@if ($paginator->hasPages())
    <!-- Pagination -->
    <div class="pagination-area d-sm-flex mt-15 justify-content-center">
        <nav aria-label="#">
            <ul class="pagination">
                {{-- Previous Page Link --}}
                @if ($paginator->onFirstPage())
                    <li class="page-item disabled">
                        <a class="page-link" href="#" style="cursor: not-allowed; ">
                            <i class="fa fa-angle-double-left"></i>
                        </a>
                    </li>
                @else
                    <li class="page-item">
                        <a class="page-link" href="{{ $paginator->previousPageUrl() }}">
                            <i class="fa fa-angle-double-left"></i>
                        </a>
                    </li>
                @endif
                @if($paginator->currentPage() > 3)
                    <li class="page-item"><a class="page-link" href="{{ $paginator->url(1)}}">1</a></li>
                @endif
                @if($paginator->currentPage() > 4)
                    <li class="page-item">
                        <span class="page-link" aria-hidden="true">...</span>
                    </li>
                @endif
                {{-- Pagination Elements --}}
                @foreach(range(1, $paginator->lastPage()) as $i)
                    @if($i >= $paginator->currentPage() - 2 && $i <= $paginator->currentPage() + 2)
                        @if ($i == $paginator->currentPage())
                            <li class="page-item active" aria-disabled="true"><span class="page-link">{{ $i }}</span>
                            </li>
                        @else
                            <li class="page-item"><a class="page-link" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                            </li>

                        @endif
                    @endif
                @endforeach

                @if($paginator->currentPage() < $paginator->lastPage() - 3)
                    <li class="page-item">
                        <span class="page-link" aria-hidden="true">...</span>
                    </li>
                @endif
                @if($paginator->currentPage() < $paginator->lastPage() - 2)
                    <li class="page-item"><a class="page-link"
                                             href="{{ $paginator->url($paginator->lastPage())}}">{{ $paginator->lastPage() }}</a>
                    </li>
                @endif

                {{-- Next Page Link --}}
                @if ($paginator->hasMorePages())
                    <li class="page-item ">
                        <a class="page-link" href="{{ $paginator->nextPageUrl() }}"> <i class="fa fa-angle-double-right"
                                                                                        aria-hidden="true"></i></a>
                    </li>
                @else
                    <li class="disabled page-item">
                        <a class="page-link" style="cursor: not-allowed; " href="#"><i
                                    class="fa fa-angle-double-right"></i></a>
                    </li>
                @endif
            </ul>
        </nav>
    </div>
    <!-- Pagination -->
@endif